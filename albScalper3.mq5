#property copyright   "Copyright 1983-2018, albCorp"
#property link        "http://www.albcorp.com"
#property version     "1.01"
#property description "albScalper 3"
#include <Trade\AccountInfo.mqh>
#include <Trade\PositionInfo.mqh>
#include <Trade\Trade.mqh>

// input parameters
input double Lot=1;  //port size
input double SL=20;  //stop loss
input double TP=40;  //take profit

#define BAR_COUNT 3
    
void OnTick()
  {    
  
    static int candleCounter;
    
    // process the orders only when a new bar is formed
    MqlRates rt[1];
    if(CopyRates(_Symbol,_Period,0,1,rt)!=1)
      {
        Print("CopyRates of ", _Symbol," failed, no history");
        return;
      }
    if(rt[0].tick_volume>1) return;
    
    candleCounter=candleCounter+1;
    
    if (candleCounter<=1) return;
    
    // get candles
    MqlRates priceData[];    
    ArraySetAsSeries(priceData,true);    
    int copied=CopyRates(_Symbol,_Period,0,BAR_COUNT,priceData);    
    
    // check signal    
    ENUM_ORDER_TYPE signal=-1;  
 
    if(priceData[1].open>priceData[1].close) // bear candlestick
      {        
        signal = ORDER_TYPE_SELL;
      }
    else
      {  
        if(priceData[1].open<priceData[1].close) // bull candle
          {
            signal = ORDER_TYPE_BUY;
          }
      }
  
    if(signal==-1) return;               
               
    Print("Signal: ",signal);
    Print("priceData[1].open: ",NormalizeDouble(priceData[1].open,_Digits));
    Print("priceData[1].close: ",NormalizeDouble(priceData[1].close,_Digits));
    // check signal    

    // check if there is an open position, and if there is, close it
    CPositionInfo posinf;
    CTrade trade;    
    double val = 0;
    double tp = 0;
    double sl = 0;
    
    if(posinf.Select(_Symbol))
      {
        Print("Position Type: ",posinf.TypeDescription());
        Print("PriceOpen: ",NormalizeDouble(posinf.PriceOpen(),_Digits));
        Print("TakeProfit: ",NormalizeDouble(posinf.TakeProfit(),_Digits));
        Print("StopLoss: ",NormalizeDouble(posinf.StopLoss(),_Digits));
        Print("PriceCurrent: ",NormalizeDouble(posinf.PriceCurrent(),_Digits));
        Print("Profit: ",NormalizeDouble(posinf.Profit(),_Digits));
        
        if((signal==ORDER_TYPE_SELL && posinf.PositionType()==POSITION_TYPE_BUY) ||
           (signal==ORDER_TYPE_BUY  && posinf.PositionType()==POSITION_TYPE_SELL))
          {
            //calculate profit/loss
            double pl = posinf.PriceOpen() - posinf.PriceCurrent();
            if(pl<0) pl = (pl * -1) * 10000;
            
            Print("PL: ", NormalizeDouble(pl,_Digits));
          
            // fecha posição somente se diferença for maior que 1 pip
            if(pl > 1)
              {
                Print("Close position");
                trade.PositionClose(_Symbol,3);
              }
            else
              {
                // set take profit/stop loss
                Print("set take profit, stop loss ????");
                return;
              }
          }
        else {
          // set take profit/stop loss                   
          if(posinf.PositionType()==POSITION_TYPE_BUY) 
            {
              if(posinf.PriceCurrent()>posinf.PriceOpen())
                {
                
                  Print("set take profit, stop loss");
                
                  val = posinf.PriceCurrent();
                  tp = val+_Point*TP;
                  sl = val-_Point*SL;
                  
                  Print("current: ", val);
                  Print("sl: ", sl);
                  Print("tp: ", tp);
                  
                  if(sl<=posinf.StopLoss()) return;                  
                  
                }
              else return;
            }
          else
            {
              if(posinf.PriceCurrent()<posinf.PriceOpen())
                {
                
                  Print("set take profit, stop loss");
                  
                  val = posinf.PriceCurrent();
                  tp = val-_Point*TP;
                  sl = val+_Point*SL;

                  Print("current: ", val);
                  Print("sl: ", sl);
                  Print("tp: ", tp);
                  
                  if(sl>=posinf.StopLoss()) return;
                }
             else return;
            }          
            
          if(!trade.PositionModify(_Symbol,sl,tp))
            Print(trade.ResultRetcodeDescription());                                
          return;
        }
      }
    // check if there is an open position, and if there is, close it
    
    // check the combination: the candle with the opposite color has formed          
    if(signal==ORDER_TYPE_BUY)
      {
        if(priceData[2].open>priceData[2].close) { //bear
        
          val = SymbolInfoDouble(_Symbol,SYMBOL_ASK);
          tp = NormalizeDouble(val+_Point*TP,_Digits);
          sl = NormalizeDouble(val-_Point*SL,_Digits);
                    
          Print("Open Buy Position");
          Print("ask: ", val);
          Print("sl: ", sl);
          Print("tp: ", tp);
          
        }
      }
    else
      {
        if(priceData[2].open<priceData[2].close) { //bull
          
          val = SymbolInfoDouble(_Symbol,SYMBOL_BID);
          tp = NormalizeDouble(val-_Point*TP,_Digits);
          sl = NormalizeDouble(val+_Point*SL,_Digits);
          
          Print("Open Sell Position");
          Print("bid: ", val);
          Print("sl: ", sl);
          Print("tp: ", tp);
        }
      }
      
    // open position
    if(val!=0)
      if(!trade.PositionOpen(_Symbol,signal,Lot,val,sl,tp))
        {
          Print(trade.ResultRetcodeDescription());
        }
          
   
  }
